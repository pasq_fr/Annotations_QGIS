<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" minScale="1e+08" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" version="3.10.8-A Coruña" styleCategories="AllStyleCategories" labelsEnabled="1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="RuleRenderer" symbollevels="0" enableorderby="0" forceraster="0">
    <rules key="{9ac1a0d0-2e77-43de-819e-7f85a55f26bc}">
      <rule symbol="0" scalemindenom="1500" scalemaxdenom="1000000" key="{d2a7d2c8-9b0c-46d0-b704-424422dc9480}"/>
      <rule symbol="1" scalemaxdenom="1500" key="{258fb294-7aef-45ad-8427-cf6e5acebf9e}"/>
    </rules>
    <symbols>
      <symbol type="marker" clip_to_extent="1" name="0" force_rhr="0" alpha="1">
        <layer locked="0" enabled="1" class="SvgMarker" pass="0">
          <prop v="0" k="angle"/>
          <prop v="255,255,255,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgd2lkdGg9IjMyLjg4NDkzbW0iCiAgIGhlaWdodD0iMzYuMTI0NDQzbW0iCiAgIHZpZXdCb3g9IjAgMCAxMTYuNTIxNCAxMjcuOTk5OTkiCiAgIGlkPSJzdmc0MjM3IgogICB2ZXJzaW9uPSIxLjEiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0MjM5IiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTQyNDIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTAiCiAgICAgc3R5bGU9ImZpbGw6I2JkYmNiYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDQxLjEgNjAuOCwzMCBjIDEuNiwwLjggMi41LDIuMiAyLjgsNCAwLjMsMS43IC0wLjIsMy4zIC0xLjQsNC42IGwgLTQxLjIsNDQuOSBjIC0xLjksMiAtNC44LDIuMSAtNywwLjQgTCA0LjYsODEuNiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDM2LjYgNjAuOCwzMC4xIGMgMS42LDAuNyAyLjUsMi4xIDIuOCwzLjkgMC4zLDEuNyAtMC4yLDMuNCAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC41IEwgNC42LDc3LjEgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU0IgogICAgIHN0eWxlPSJmaWxsOiNlOWU5ZTk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gNDkuMSwzMi4xIDYwLjgsMzAuMSBjIDEuNiwwLjggMi41LDIuMiAyLjgsMy45IDAuMywxLjggLTAuMiwzLjQgLTEuNCw0LjcgbCAtNDEuMiw0NC44IGMgLTEuOSwyLjEgLTQuOCwyLjIgLTcsMC41IEwgNC42LDcyLjYgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU2IgogICAgIGQ9Im0gNDkuMSwyNy43IDYwLjgsMzAgYyAxLjYsMC44IDIuNSwyLjIgMi44LDQgMC4zLDEuNyAtMC4yLDMuMyAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC40IEwgNC42LDY4LjIgWiIgZmlsbD0icGFyYW0oZmlsbCkiIGZpbGwtb3BhY2l0eT0icGFyYW0oZmlsbC1vcGFjaXR5KSIgc3Ryb2tlPSJwYXJhbShvdXRsaW5lKSIgc3Ryb2tlLW9wYWNpdHk9InBhcmFtKG91dGxpbmUtb3BhY2l0eSkiIHN0cm9rZS13aWR0aD0icGFyYW0ob3V0bGluZS13aWR0aCkiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ1OCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDM5LDM1LjYgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjEsLTIuNCA0LjMsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMSw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDI5LjUsNDQuNSBjIC0wLjgsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMSwtMS4xIDIuOCwxLjkgNC4yLDUuMyAzLjEsNy43IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjIiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxOS45LDUzLjUgYyAtMC43LC0xLjYgLTAuOCwtMy4yIC0wLjIsLTQuNSAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwLjQsNjIuNCBjIC0wLjcsLTEuNiAtMC44LC0zLjIgLTAuMiwtNC41IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMSAyLjgsMS44IDQuMSw1LjIgMyw3LjYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDQuNyw3NS42IEMgNC42LDc1LjUgNC41LDc1LjUgNC40LDc1LjQgMS41LDczLjYgMC4yLDcwLjEgMS4zLDY3LjcgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjgiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0My40LDMwLjcgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDMzLjksMzkuNiBjIC0wLjcsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMS4xIDIuOCwxLjkgNC4xLDUuMyAzLDcuNyIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDcyIgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjQuNCw0OC42IGMgLTAuOCwtMS42IC0wLjgsLTMuMiAtMC4yLC00LjUgMS4xLC0yLjQgNC4zLC0yLjkgNy4xLC0xLjEgMi44LDEuOSA0LjIsNS4zIDMuMSw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDE0LjgsNTcuNSBDIDE0LjEsNTUuOSAxNCw1NC4zIDE0LjYsNTMgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NzYiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA2LDY3LjMgQyA1LjIsNjUuNyA1LjIsNjQuMSA1LjgsNjIuOCBjIDEuMSwtMi40IDQuMywtMi45IDcuMSwtMSAyLjgsMS44IDQuMiw1LjIgMy4xLDcuNiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDc4IgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gMjUuNiw3Ni40IDQ1LjIsNTYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDM0LjEsNzkuMyA0NC4yLDY4LjgiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDUwLjYsNjEuMyA2MS43LDQ5LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDY5LjIsNTYgNzksNTIuNSA5NS4yLDU5LjYgNjMuNyw5Ni45IFoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NiIKICAgICBzdHlsZT0iZmlsbDojZDNkMmQyO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTpub25lIgogICAgIGQ9Ik0gOTQuNCw1OS4yIDc0LjMsODMgYyAwLDAgLTMuMSwxLjMgLTIuNCwtMC4xIEwgODQuMSw1OS40IGMgNiwtMy4yIDguMiwtMy41IDEwLjMsLTAuMiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDg4IgogICAgIHN0eWxlPSJmaWxsOiM5MThmOTA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTEzLjUsMTUuMjEgLTExLjksMC41OSBjIDAsMCAtMTcuNjYsMzguNDggLTE4LjUsNDUuNTEgLTAuNjEsNS4wOCA4LjY4LC02LjcyIDExLjYsLTEuMTEgMC4zLDMuOCAxOC44LC00NC45OSAxOC44LC00NC45OSB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTAiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSAxMDEuNiwxNS44IDg4LjIsOS44IDY5LjIsNTYgYyAtMS40LDMuNCAxMi41LC02LjQ4IDEzLjksNS42MiAtMS4yLDIuOSAxMi44LC0zMS43MiAxOC41LC00NS44MiB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA5MC4zLDguMiA4Ny4xLDYuMDExIGMgMCwwIC03Ljk2LDE4Ljc0OSAtMTYuMDgsMzguNTE5IDAsMCAtMC45NSw1LjM2IC0xLjc4LDExLjE2IEMgNjguMzQsNjEuOTMgOTAuMyw4LjIgOTAuMyw4LjIgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDk0IgogICAgIHN0eWxlPSJmaWxsOiMyMzFmMjA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gNzUuNiw4Mi44IDYzLjcsOTYuOSBjIDAuNywtNS44IDEuNSwtMTEuNiAyLjMsLTE3LjQgMi44LDIuOSA2LjEsNCA5LjYsMy4zIHoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ5OCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwNC43LDQuMiBjIDcsMy4yIDEwLjksOC4zIDguNiwxMS42IC0yLjIsMy4yIC05LjcsMy4zIC0xNi43LDAuMSBDIDg5LjYsMTIuOCA4NS44LDcuNiA4OCw0LjMgOTAuMywxLjEgOTcuOCwxIDEwNC43LDQuMiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg1MDAiCiAgICAgc3R5bGU9ImZpbGw6IzIzMWYyMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMDEuOSw4LjMgYyAyLjEsMSAzLjIsMi41IDIuNiwzLjUgLTAuNywwLjkgLTIuOSwxIC01LDAgLTIuMSwtMC45IC0zLjMsLTIuNSAtMi42LC0zLjUgMC43LC0wLjkgMi45LC0wLjkgNSwwIHoiIC8+Cjwvc3ZnPgo=" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0,0,0,255" k="outline_color"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="8" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="marker" clip_to_extent="1" name="1" force_rhr="0" alpha="1">
        <layer locked="0" enabled="1" class="SvgMarker" pass="0">
          <prop v="0" k="angle"/>
          <prop v="255,255,255,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgd2lkdGg9IjMyLjg4NDkzbW0iCiAgIGhlaWdodD0iMzYuMTI0NDQzbW0iCiAgIHZpZXdCb3g9IjAgMCAxMTYuNTIxNCAxMjcuOTk5OTkiCiAgIGlkPSJzdmc0MjM3IgogICB2ZXJzaW9uPSIxLjEiPgogIDxkZWZzCiAgICAgaWQ9ImRlZnM0MjM5IiAvPgogIDxtZXRhZGF0YQogICAgIGlkPSJtZXRhZGF0YTQyNDIiPgogICAgPHJkZjpSREY+CiAgICAgIDxjYzpXb3JrCiAgICAgICAgIHJkZjphYm91dD0iIj4KICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD4KICAgICAgICA8ZGM6dHlwZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+CiAgICAgICAgPGRjOnRpdGxlPjwvZGM6dGl0bGU+CiAgICAgIDwvY2M6V29yaz4KICAgIDwvcmRmOlJERj4KICA8L21ldGFkYXRhPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTAiCiAgICAgc3R5bGU9ImZpbGw6I2JkYmNiYztmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDQxLjEgNjAuOCwzMCBjIDEuNiwwLjggMi41LDIuMiAyLjgsNCAwLjMsMS43IC0wLjIsMy4zIC0xLjQsNC42IGwgLTQxLjIsNDQuOSBjIC0xLjksMiAtNC44LDIuMSAtNywwLjQgTCA0LjYsODEuNiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0OS4xLDM2LjYgNjAuOCwzMC4xIGMgMS42LDAuNyAyLjUsMi4xIDIuOCwzLjkgMC4zLDEuNyAtMC4yLDMuNCAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC41IEwgNC42LDc3LjEgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU0IgogICAgIHN0eWxlPSJmaWxsOiNlOWU5ZTk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gNDkuMSwzMi4xIDYwLjgsMzAuMSBjIDEuNiwwLjggMi41LDIuMiAyLjgsMy45IDAuMywxLjggLTAuMiwzLjQgLTEuNCw0LjcgbCAtNDEuMiw0NC44IGMgLTEuOSwyLjEgLTQuOCwyLjIgLTcsMC41IEwgNC42LDcyLjYgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDU2IgogICAgIGQ9Im0gNDkuMSwyNy43IDYwLjgsMzAgYyAxLjYsMC44IDIuNSwyLjIgMi44LDQgMC4zLDEuNyAtMC4yLDMuMyAtMS40LDQuNiBsIC00MS4yLDQ0LjkgYyAtMS45LDIgLTQuOCwyLjEgLTcsMC40IEwgNC42LDY4LjIgWiIgZmlsbD0icGFyYW0oZmlsbCkiIGZpbGwtb3BhY2l0eT0icGFyYW0oZmlsbC1vcGFjaXR5KSIgc3Ryb2tlPSJwYXJhbShvdXRsaW5lKSIgc3Ryb2tlLW9wYWNpdHk9InBhcmFtKG91dGxpbmUtb3BhY2l0eSkiIHN0cm9rZS13aWR0aD0icGFyYW0ob3V0bGluZS13aWR0aCkiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ1OCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDM5LDM1LjYgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjEsLTIuNCA0LjMsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMSw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDI5LjUsNDQuNSBjIC0wLjgsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMSwtMS4xIDIuOCwxLjkgNC4yLDUuMyAzLjEsNy43IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjIiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxOS45LDUzLjUgYyAtMC43LC0xLjYgLTAuOCwtMy4yIC0wLjIsLTQuNSAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwLjQsNjIuNCBjIC0wLjcsLTEuNiAtMC44LC0zLjIgLTAuMiwtNC41IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMSAyLjgsMS44IDQuMSw1LjIgMyw3LjYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ2NiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDQuNyw3NS42IEMgNC42LDc1LjUgNC41LDc1LjUgNC40LDc1LjQgMS41LDczLjYgMC4yLDcwLjEgMS4zLDY3LjcgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NjgiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSA0My40LDMwLjcgYyAtMC43LC0xLjUgLTAuOCwtMy4xIC0wLjIsLTQuNCAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEuMSAyLjgsMS45IDQuMiw1LjMgMyw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDMzLjksMzkuNiBjIC0wLjcsLTEuNSAtMC44LC0zLjEgLTAuMiwtNC40IDEuMSwtMi40IDQuMywtMi45IDcuMiwtMS4xIDIuOCwxLjkgNC4xLDUuMyAzLDcuNyIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDcyIgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMjQuNCw0OC42IGMgLTAuOCwtMS42IC0wLjgsLTMuMiAtMC4yLC00LjUgMS4xLC0yLjQgNC4zLC0yLjkgNy4xLC0xLjEgMi44LDEuOSA0LjIsNS4zIDMuMSw3LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ3NCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDE0LjgsNTcuNSBDIDE0LjEsNTUuOSAxNCw1NC4zIDE0LjYsNTMgYyAxLjIsLTIuNCA0LjQsLTIuOSA3LjIsLTEgMi44LDEuOCA0LjIsNS4yIDMsNy42IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0NzYiCiAgICAgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA2LDY3LjMgQyA1LjIsNjUuNyA1LjIsNjQuMSA1LjgsNjIuOCBjIDEuMSwtMi40IDQuMywtMi45IDcuMSwtMSAyLjgsMS44IDQuMiw1LjIgMy4xLDcuNiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDc4IgogICAgIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gMjUuNiw3Ni40IDQ1LjIsNTYiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MCIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDM0LjEsNzkuMyA0NC4yLDY4LjgiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4MiIKICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDUwLjYsNjEuMyA2MS43LDQ5LjciIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJNIDY5LjIsNTYgNzksNTIuNSA5NS4yLDU5LjYgNjMuNyw5Ni45IFoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ4NiIKICAgICBzdHlsZT0iZmlsbDojZDNkMmQyO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTpub25lIgogICAgIGQ9Ik0gOTQuNCw1OS4yIDc0LjMsODMgYyAwLDAgLTMuMSwxLjMgLTIuNCwtMC4xIEwgODQuMSw1OS40IGMgNiwtMy4yIDguMiwtMy41IDEwLjMsLTAuMiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDg4IgogICAgIHN0eWxlPSJmaWxsOiM5MThmOTA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Im0gMTEzLjUsMTUuMjEgLTExLjksMC41OSBjIDAsMCAtMTcuNjYsMzguNDggLTE4LjUsNDUuNTEgLTAuNjEsNS4wOCA4LjY4LC02LjcyIDExLjYsLTEuMTEgMC4zLDMuOCAxOC44LC00NC45OSAxOC44LC00NC45OSB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTAiCiAgICAgc3R5bGU9ImZpbGw6I2ZmZmZmZjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSAxMDEuNiwxNS44IDg4LjIsOS44IDY5LjIsNTYgYyAtMS40LDMuNCAxMi41LC02LjQ4IDEzLjksNS42MiAtMS4yLDIuOSAxMi44LC0zMS43MiAxOC41LC00NS44MiB6IiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg0OTIiCiAgICAgc3R5bGU9ImZpbGw6I2QzZDJkMjtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0iTSA5MC4zLDguMiA4Ny4xLDYuMDExIGMgMCwwIC03Ljk2LDE4Ljc0OSAtMTYuMDgsMzguNTE5IDAsMCAtMC45NSw1LjM2IC0xLjc4LDExLjE2IEMgNjguMzQsNjEuOTMgOTAuMyw4LjIgOTAuMyw4LjIgWiIgLz4KICA8cGF0aAogICAgIGlkPSJwYXRoNDk0IgogICAgIHN0eWxlPSJmaWxsOiMyMzFmMjA7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMyMzFmMjA7c3Ryb2tlLXdpZHRoOjEuODU1NDtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjptaXRlcjtzdHJva2UtbWl0ZXJsaW1pdDoyMi45MjU2O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgIGQ9Ik0gNzUuNiw4Mi44IDYzLjcsOTYuOSBjIDAuNywtNS44IDEuNSwtMTEuNiAyLjMsLTE3LjQgMi44LDIuOSA2LjEsNCA5LjYsMy4zIHoiIC8+CiAgPHBhdGgKICAgICBpZD0icGF0aDQ5OCIKICAgICBzdHlsZT0iZmlsbDojZTllOWU5O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMjMxZjIwO3N0cm9rZS13aWR0aDoxLjg1NTQ7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW1pdGVybGltaXQ6MjIuOTI1NjtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICBkPSJtIDEwNC43LDQuMiBjIDcsMy4yIDEwLjksOC4zIDguNiwxMS42IC0yLjIsMy4yIC05LjcsMy4zIC0xNi43LDAuMSBDIDg5LjYsMTIuOCA4NS44LDcuNiA4OCw0LjMgOTAuMywxLjEgOTcuOCwxIDEwNC43LDQuMiBaIiAvPgogIDxwYXRoCiAgICAgaWQ9InBhdGg1MDAiCiAgICAgc3R5bGU9ImZpbGw6IzIzMWYyMDtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzIzMWYyMDtzdHJva2Utd2lkdGg6MS44NTU0O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1taXRlcmxpbWl0OjIyLjkyNTY7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgZD0ibSAxMDEuOSw4LjMgYyAyLjEsMSAzLjIsMi41IDIuNiwzLjUgLTAuNywwLjkgLTIuOSwxIC01LDAgLTIuMSwtMC45IC0zLjMsLTIuNSAtMi42LC0zLjUgMC43LC0wLjkgMi45LC0wLjkgNSwwIHoiIC8+Cjwvc3ZnPgo=" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0.2" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="fillColor">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style namedStyle="Regular" fontSizeUnit="Point" fontUnderline="0" fontSize="10" isExpression="0" textOpacity="1" fontStrikeout="0" fieldName="texte" multilineHeight="1" fontWordSpacing="0.1875" fontLetterSpacing="0.1875" fontCapitals="0" blendMode="0" fontItalic="0" useSubstitutions="0" previewBkgrdColor="255,255,255,255" textColor="0,0,0,255" fontKerning="1" textOrientation="horizontal" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontWeight="25" fontFamily="RobotoLight">
        <text-buffer bufferColor="255,255,255,255" bufferSize="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferOpacity="1" bufferSizeUnits="MM" bufferJoinStyle="128" bufferDraw="0" bufferBlendMode="0" bufferNoFill="1"/>
        <background shapeJoinStyle="128" shapeOffsetUnit="MM" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="1" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeRadiiX="1" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeOffsetX="0" shapeRadiiY="1" shapeBorderColor="227,26,28,255" shapeOpacity="1" shapeSizeType="0" shapeSizeX="1" shapeSizeUnit="MM" shapeRotationType="0" shapeRadiiUnit="MM" shapeRotation="0" shapeDraw="1" shapeSVGFile="" shapeBorderWidth="0.3" shapeFillColor="255,255,255,255" shapeBlendMode="0">
          <symbol type="marker" clip_to_extent="1" name="markerSymbol" force_rhr="0" alpha="1">
            <layer locked="0" enabled="1" class="SimpleMarker" pass="0">
              <prop v="0" k="angle"/>
              <prop v="231,113,72,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowScale="100" shadowOffsetAngle="135" shadowOffsetGlobal="1" shadowRadiusAlphaOnly="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="1" shadowOffsetDist="1" shadowOffsetUnit="MM" shadowColor="0,0,0,255" shadowRadiusUnit="MM" shadowUnder="0" shadowOpacity="0.7" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowRadius="1.5"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format reverseDirectionSymbol="0" wrapChar="" decimals="3" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" multilineAlign="3" plussign="0" rightDirectionSymbol=">" leftDirectionSymbol="&lt;" addDirectionSymbol="0" autoWrapLength="0" formatNumbers="0"/>
      <placement placement="0" offsetType="0" preserveRotation="1" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" distUnits="MM" fitInPolygonOnly="0" xOffset="0" overrunDistanceUnit="MM" rotationAngle="0" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGenerator="" placementFlags="10" layerType="PointGeometry" overrunDistance="0" maxCurvedCharAngleOut="-25" yOffset="0" distMapUnitScale="3x:0,0,0,0,0,0" priority="5" repeatDistance="0" repeatDistanceUnits="MM" dist="5" quadOffset="4" geometryGeneratorEnabled="0" maxCurvedCharAngleIn="25" geometryGeneratorType="PointGeometry" centroidInside="0" centroidWhole="0"/>
      <rendering maxNumLabels="2000" limitNumLabels="0" scaleMin="0" scaleMax="1501" drawLabels="1" fontMinPixelSize="3" zIndex="5" scaleVisibility="1" fontMaxPixelSize="10000" fontLimitPixelSize="0" obstacle="1" displayAll="0" obstacleType="0" obstacleFactor="1" labelPerPart="0" upsidedownLabels="0" mergeLines="0" minFeatureSize="0"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" value="" name="name"/>
          <Option type="Map" name="properties">
            <Option type="Map" name="LabelRotation">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="rot_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="PositionX">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="x_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="PositionY">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="y_label" name="field"/>
              <Option type="int" value="2" name="type"/>
            </Option>
            <Option type="Map" name="ShapeBorderColor">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FF0000', '#000000')" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
            <Option type="Map" name="ShapeFillColor">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="if(&quot;important&quot; IS TRUE, '#FFD800', '#FFFFFF')" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
          </Option>
          <Option type="QString" value="collection" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
          <Option type="bool" value="false" name="drawToAllParts"/>
          <Option type="QString" value="1" name="enabled"/>
          <Option type="QString" value="&lt;symbol type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;layer locked=&quot;0&quot; enabled=&quot;1&quot; class=&quot;SimpleLine&quot; pass=&quot;0&quot;>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;82,82,82,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
          <Option type="double" value="0.2" name="minLength"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
          <Option type="QString" value="MM" name="minLengthUnit"/>
          <Option type="double" value="1" name="offsetFromAnchor"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
          <Option type="double" value="1.9999999999999998" name="offsetFromLabel"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property value="texte" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory minimumSize="0" rotationOffset="270" lineSizeType="MM" labelPlacementMethod="XHeight" diagramOrientation="Up" backgroundColor="#ffffff" scaleDependency="Area" minScaleDenominator="0" height="15" penColor="#000000" enabled="0" sizeScale="3x:0,0,0,0,0,0" width="15" barWidth="5" scaleBasedVisibility="0" maxScaleDenominator="1e+08" lineSizeScale="3x:0,0,0,0,0,0" opacity="1" penWidth="0" backgroundAlpha="255" sizeType="MM" penAlpha="255">
      <fontProperties style="" description="Roboto,11,-1,5,25,0,0,0,0,0"/>
      <attribute color="#000000" label="" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="0" linePlacementFlags="18" priority="0" dist="0" showAll="1" obstacle="0" zIndex="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="id_note">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="texte">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option type="bool" value="true" name="IsMultiline"/>
            <Option type="bool" value="false" name="UseHtml"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="important">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option type="QString" value="" name="CheckedState"/>
            <Option type="QString" value="" name="UncheckedState"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="date_creation">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="date_modification">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="user_creation">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="user_modification">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="x_label">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="y_label">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="rot_label">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id_note"/>
    <alias name="" index="1" field="texte"/>
    <alias name="" index="2" field="important"/>
    <alias name="" index="3" field="date_creation"/>
    <alias name="" index="4" field="date_modification"/>
    <alias name="" index="5" field="user_creation"/>
    <alias name="" index="6" field="user_modification"/>
    <alias name="" index="7" field="x_label"/>
    <alias name="" index="8" field="y_label"/>
    <alias name="" index="9" field="rot_label"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="id_note" expression="" applyOnUpdate="0"/>
    <default field="texte" expression="" applyOnUpdate="0"/>
    <default field="important" expression="" applyOnUpdate="0"/>
    <default field="date_creation" expression="now()" applyOnUpdate="0"/>
    <default field="date_modification" expression="now()" applyOnUpdate="1"/>
    <default field="user_creation" expression=" @user_full_name " applyOnUpdate="0"/>
    <default field="user_modification" expression=" @user_full_name " applyOnUpdate="1"/>
    <default field="x_label" expression="" applyOnUpdate="0"/>
    <default field="y_label" expression="" applyOnUpdate="0"/>
    <default field="rot_label" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" notnull_strength="1" exp_strength="0" constraints="3" field="id_note"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="texte"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="important"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="date_creation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="date_modification"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="user_creation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="user_modification"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="x_label"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="y_label"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="rot_label"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="id_note" exp=""/>
    <constraint desc="" field="texte" exp=""/>
    <constraint desc="" field="important" exp=""/>
    <constraint desc="" field="date_creation" exp=""/>
    <constraint desc="" field="date_modification" exp=""/>
    <constraint desc="" field="user_creation" exp=""/>
    <constraint desc="" field="user_modification" exp=""/>
    <constraint desc="" field="x_label" exp=""/>
    <constraint desc="" field="y_label" exp=""/>
    <constraint desc="" field="rot_label" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column width="-1" type="field" name="id_note" hidden="0"/>
      <column width="674" type="field" name="texte" hidden="0"/>
      <column width="82" type="field" name="important" hidden="0"/>
      <column width="167" type="field" name="date_creation" hidden="0"/>
      <column width="-1" type="field" name="date_modification" hidden="0"/>
      <column width="-1" type="field" name="user_creation" hidden="0"/>
      <column width="-1" type="field" name="user_modification" hidden="0"/>
      <column width="-1" type="field" name="x_label" hidden="0"/>
      <column width="-1" type="field" name="y_label" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
      <column width="-1" type="field" name="rot_label" hidden="0"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="0" name="date_creation"/>
    <field editable="0" name="date_modification"/>
    <field editable="0" name="id_note"/>
    <field editable="1" name="important"/>
    <field editable="0" name="rot_label"/>
    <field editable="1" name="texte"/>
    <field editable="0" name="user_creation"/>
    <field editable="0" name="user_modification"/>
    <field editable="0" name="x_label"/>
    <field editable="0" name="y_label"/>
  </editable>
  <labelOnTop>
    <field name="date_creation" labelOnTop="0"/>
    <field name="date_modification" labelOnTop="0"/>
    <field name="id_note" labelOnTop="0"/>
    <field name="important" labelOnTop="0"/>
    <field name="rot_label" labelOnTop="0"/>
    <field name="texte" labelOnTop="0"/>
    <field name="user_creation" labelOnTop="0"/>
    <field name="user_modification" labelOnTop="0"/>
    <field name="x_label" labelOnTop="0"/>
    <field name="y_label" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>texte</previewExpression>
  <mapTip>[% "texte" %]
&lt;/br>
modif par [% "user_modification" %] le [% "date_modification" %]</mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
